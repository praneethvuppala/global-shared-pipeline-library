List<String> call(pipelineParams) {
	switch(pipelineParams.buildType) {
		case 'Npm':
			GlbNpmDeploy(pipelineParams)
			break
		case 'Maven':
			GlbMavenDeploy(pipelineParams)
			break
		default:
			error("Please provide right buildType. These are build types available Npm and Maven. If you didn't see build type you need, then contact DevOps team ")
			break
	}
}
this
