List<String> call(pipelineParams) {
	switch(pipelineParams.buildType) {
		case 'Gradle':
			GlbGradleSonarScan(pipelineParams)
			break
		case 'Maven':
			GlbMavenSonarScan(pipelineParams)
			break
		case 'Npm':
			GlbNpmSonarScan(pipelineParams)
			break
		default:
			error("Please provide right buildType to Sonar scan. These are build types available Gradle, Npm, Maven. If you didn't see build type you need, then contact DevOps team")
			break
	}
}
this
