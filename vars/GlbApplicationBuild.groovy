List<String> call(pipelineParams) {
	switch(pipelineParams.buildType) {
		case 'Gradle':
			GlbGradleBuild(pipelineParams)
			break
		case 'Maven':
			GlbMavenBuild(pipelineParams)
			break
		case 'Npm':
			GlbNpmBuild(pipelineParams)
			break
		default:
			error("Please provide right buildType. These are build types available Gradle, Npm, Maven. If you didn't see build type you need, then contact DevOps team ")
			break
	}
}
this
