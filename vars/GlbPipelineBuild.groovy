def call(body) {
	def pipelineParams=[:]
	body.resolveStrategy=Closure.DELEGATE_FIRST
	body.delegate=pipelineParams
	body()
	pipeline{
		agent any
		 options {
			disableConcurrentBuilds()
			buildDiscarder(logRotator(numToKeepStr: '30'))
			timestamps()
		}
		stages{
			stage('CleanWorkspace') {
				steps {
					cleanWs()
				}
			}//end of Clean Workspace
			stage('checkout SCM') {
				steps{
					checkout scm
				}
			}//end of checkout
			stage('Set Defaults'){
				steps{
					GlbDefaultValues(pipelineParams)
				}
			}//end of Default values
			stage('Build Application'){
				steps{
					GlbApplicationBuild(pipelineParams)					
				}
			}//end of build
			stage("SonarQube Scan"){
				steps{
					GlbSonarScan(pipelineParams)			
				}
			}
			stage("Publish Artifacts"){
				steps{
					GlbArtifactoryPublish(pipelineParams)	
				}
			}
			stage("Deploy"){
				when{
					expression {return pipelineParams.deploy}
				}
				steps{
					GlbApplicationDeploy(pipelineParams)	
				}
			}			
		}//end of stages
		post{
			always{
				//GlbNotificationUtil(pipelineParams.recipientEmail)
				print("Send email here")
			}
			cleanup{
				cleanWs()
			}
		}//end of post
	}//end of pipeline
}
