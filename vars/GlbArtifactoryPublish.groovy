List<String> call(pipelineParams) {
	switch(pipelineParams.publishType) {
		case 'Gradle':
			GlbGradlePublish(pipelineParams)
			break
		case 'Maven':
			GlbMavenPublish(pipelineParams)
			break
		case 'Docker':
			GlbDockerPublish(pipelineParams)
			break
		default:
			error("Please provide right publish type. These are build types available Gradle, Maven and Docker. If you didn't see build type you need, then contact DevOps team ")
			break
	}
}
this
